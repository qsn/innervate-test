import moment from 'moment';
import crypto from 'crypto';
import isEmpty from 'lodash/isEmpty'
import {oncePerServices, missingService} from '../../common/services/index';

function apolloToRelayResolverAdapter(oldResolver) {
  return function (obj, args, context) {
    return oldResolver(args, context.request);
  }
}

export default oncePerServices(function (services) {
    
    const {
        postgres = missingService('postgres')
    } = services;
  
  function getUsers(builderContext) {
    return async function(obj, args, context) {
        let rows = Object.create(null);

        let where = '';
        let params = [];

        if (!isEmpty(args)) {
            if (args.hasOwnProperty('blocked')) {
                params.push(args.blocked);
                where = `${where ? `${where} AND ` : ''} blocked = $${params.length}`;
            }

            if (args.hasOwnProperty('manager')) {
                params.push(args.manager);
                where = `${where ? `${where} AND ` : ''} manager = $${params.length}`;
            }

            if (args.hasOwnProperty('search')) {
                params.push(`%${args.search}%`);
                where = `${where ? `${where} AND ` : ''} (login LIKE $${params.length} OR name LIKE $${params.length})`;
            }

            if (params.length) {
                where = ` WHERE${where}`;
            }
        }

        ({rows} = await postgres.exec({statement: `SELECT * FROM users${where}`, params}));

        return rows;
    }
  }

    function auth(builderContext) {
        return async function(obj, args, context) {
            if (args.hasOwnProperty('login') && args.hasOwnProperty('password')) {
                let {rows} = await postgres.exec({statement: `SELECT login, password_hash FROM users WHERE login = $1`, params: [args.login]});

                if (rows.length && checkPassword(args.password, rows[0].password_hash)) {
                    return [{
                        result: 'Успех'
                    }];
                } else {
                    return [{
                        result: 'Ошибка',
                        error: 'Такого аккаунта не существует, либо пароль неверный',
                    }];
                }
            }

            return [{
                result: 'Ошибка',
                error: 'Неверные параметры',
            }];
        }
    }

    function checkPassword(password, passwordHash) {
        return crypto.createHash('md5').update(password).digest('hex') === passwordHash;
    }

  return {
    getUsers,
    auth
  }
});
