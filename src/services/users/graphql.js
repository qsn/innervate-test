import {oncePerServices, missingService} from '../../common/services/index'

const PREFIX = '';

export default oncePerServices(function (services) {
  
  const graphqlBuilderSchema = require('../../common/graphql/LevelBuilder.schema');
  
  const resolvers = require('./resolvers').default(services);
  
  return async function builder(args) {
    
    graphqlBuilderSchema.build_options(args);
    const { parentLevelBuilder, typeDefs, builderContext } = args;
    
    typeDefs.push(`
      scalar JSON
    
      type ${PREFIX}User {
        user_id: Int,
        login: String,
        name: String,
        email: String,
        manager: Boolean,
        blocked: Boolean, 
        data: JSON
      }
      
      type ${PREFIX}Auth {
        result: String
        error: String
      }
      
    `);
    
    parentLevelBuilder.addQuery({
          name: `getUsers`,
          type: `[${PREFIX}User]`,
          args: `
        blocked: Boolean,
        manager: Boolean,
        search: String
      `,
          resolver: resolvers.getUsers(builderContext),
      });

    parentLevelBuilder.addMutation({
        name: `auth`,
        type: `[${PREFIX}Auth]`,
        args: `
       login: String,
       password: String
    `,
        resolver: resolvers.auth(builderContext),
    });
    
  }
});
