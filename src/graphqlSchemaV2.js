import {oncePerServices} from './common/services'
import {makeExecutableSchema} from 'graphql-tools'
import {SchemaBuilder, LevelBuilder} from './common/graphql'

const hasOwnProperty = Object.prototype.hasOwnProperty;

import {missingArgument, invalidArgument} from './common/utils/arguments'
import GraphQLJSON from "graphql-type-json";

export default oncePerServices(function (services = missingArgument('services')) {

  return async function() {
    const typeDefs = [];
    const resolvers = Object.create(null);

    // Добавляем определение скаляра JSON
    resolvers.JSON = GraphQLJSON;

    await (new SchemaBuilder({
      // test: require('./services/test/graphql').default(services),
      users: require('./services/users/graphql').default(services),
    }).build({typeDefs, resolvers}));


    return makeExecutableSchema({
      typeDefs,
      resolvers
    })
  }
});

